﻿using Microsoft.AspNetCore.SignalR;

namespace WebApi
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message); // Gửi tin nhắn tới tất cả các client đang kết nối
        }
        public async Task SendMessageToUser(string userSend, string userReceive, string message)
        {
            try
            {
                await Clients.Client(userReceive).SendAsync("ReceiveMessageToUser", userSend, message);
                await CallAPI();
            }
            catch (Exception ex)
            {
            }
        }
        public string GetConnectionId() => Context.ConnectionId;
    
        private async Task<string> CallAPI()
        {
            var client = new HttpClient();
            var responseApi = await client.PostAsync("https://localhost:7223/WeatherForecast/PostChecker", null);
            var rpStr = await responseApi.Content.ReadAsStringAsync();
            return rpStr;
        }
    }
}
