﻿using Microsoft.AspNetCore.SignalR;

namespace chatSignalR
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message); // Gửi tin nhắn tới tất cả các client đang kết nối
        }
        public async Task SendMessageToUser(string userSend, string userReceive, string message)
        {
            try
            {
                await Clients.Client(userReceive).SendAsync("ReceiveMessageToUser", userSend, message);
                            
            }
            catch (Exception ex)
            {
            }
        }
        public string GetConnectionId() => Context.ConnectionId;
    
        private async Task CallAPI()
        {
            var client = new HttpClient();
            var responseApi = await client.PostAsync("https://localhost:7214/api/WeatherForecast/GetCheck", null);
            var responseString = await responseApi.Content.ReadAsStringAsync();

        }
    }
}
